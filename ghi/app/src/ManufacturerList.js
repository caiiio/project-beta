import React from 'react';


function ManufacturerList({manufacturers, getManufacturers}) {




    return (
        <table className="table table-striped table-hover align-middle mt-5">
          <thead>
            <tr>
              <th>Manufacturer</th>


            </tr>
          </thead>
          <tbody>
            {manufacturers.map(manufacturer => {
                return (
                    <tr key={manufacturer.id}>
                        <td>{ manufacturer.name }</td>
                    </tr>
                );
            })}
          </tbody>
        </table>

    )
}

export default ManufacturerList;
