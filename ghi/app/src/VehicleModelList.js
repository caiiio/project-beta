import React from 'react';

function VehicleModelList({vehiclemodels, manufacturers}) {

return (
    <table className="table table-striped table-hover align-middle mt-5">
        <thead>
            <tr>
                <th>Manufacturer</th>
                <th>Vehicle Model</th>
                <th>Picture</th>
            </tr>
        </thead>
        <tbody>
            {vehiclemodels.map(model => {
                return (
                    <tr key={model.href}>
                        <td>{model.manufacturer.name}</td>
                        <td>{model.name}</td>
                        <td><img src={model.picture_url} className="img-thumbnail shoes" width="300"></img></td>
                    </tr>
                );
            })}
        </tbody>
    </table>

)
}

export default VehicleModelList;
