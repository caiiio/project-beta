import React, { useEffect, useState } from 'react';

function SalesPersonForm(props) {
    const [name, setName] = useState('');
    const [employee_id, setEmployeeNumber] = useState('');

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const handleEmployeeNumberChange = (event) => {
        const value = event.target.value;
        setEmployeeNumber(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};

        data.name = name;
        data.employee_id = employee_id;

        const employeeUrl = "http://localhost:8090/api/employees/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };

        const response = await fetch(employeeUrl, fetchConfig);
        if (response.ok) {
            const newSalesperson = await response.json();

            setName("");
            setEmployeeNumber("");
        }
    }

    return (
        <div className="my-5 container">
            <div className="row">
                <div className="col col-sm-auto">
                </div>
                <div className="col">
                    <div className="card shadow">
                        <div className="card-body">
                            <form onSubmit={handleSubmit} id="create-employee-form">
                                <h1 className="card-title">New Salesperson Form</h1>
                                <div className="row">
                                    <div className="col">

                                        <div className="form-floating mb-3">
                                            <input onChange={handleNameChange} value={name} required placeholder="employee name" type="text" id="name" name="name" className="form-control" />
                                            <label htmlFor="name">Name</label>
                                        </div>

                                        <div className="form-floating mb-3">
                                            <input onChange={handleEmployeeNumberChange} value={employee_id} required placeholder="employee number" type="employee_id" id="employee_id" name="employee_id" className="form-control" />
                                            <label htmlFor="employee_id">Employee ID</label>
                                        </div>
                                    </div>
                                </div>
                                <button className="btn btn-lg btn-primary">Create SalesPerson</button>
                            </form>
                            <div className="alert alert-success d-none mb-0" id="success-message">
                Congratulations!</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );

}

export default SalesPersonForm;
