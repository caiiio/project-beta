import React, { useEffect, useState } from 'react';

function CustomerForm(props) {
    const [name, setName] = useState('');
    const [address, setAddress] = useState('');
    const [phone_number, setPhoneNumber] = useState('');

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const handleAddressChange = (event) => {
        const value = event.target.value;
        setAddress(value);
    }

    const handlePhoneNumber = (event) => {
        const value = event.target.value;
        setPhoneNumber(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};

        data.name = name;
        data.address = address;
        data.phone_number = phone_number;

        const saleUrl = "http://localhost:8090/api/sales/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };

        const response = await fetch(saleUrl, fetchConfig);
        if (response.ok) {
            const newSalesperson = await response.json();

            setName("");
            setAddress("");
            setPhoneNumber("");
        }
    }

    return (
        <div className="my-5 container">
            <div className="row">
                <div className="col col-sm-auto">
                </div>
                <div className="col">
                    <div className="card shadow">
                        <div className="card-body">
                            <form onSubmit={handleSubmit} id="create-employee-form">
                                <h1 className="card-title">New Customer Form</h1>
                                <div className="row">
                                    <div className="col">

                                        <div className="form-floating mb-3">
                                            <input onChange={handleNameChange} value={name} required placeholder="Name" type="text" id="name" name="name" className="form-control" />
                                            <label htmlFor="name">Name</label>
                                        </div>

                                        <div className="form-floating mb-3">
                                            <input onChange={handleAddressChange} value={address} required placeholder="Address" type="text" id="address" name="address" className="form-control" />
                                            <label htmlFor="employee_id">Address</label>
                                        </div>

                                        <div className="form-floating mb-3">
                                            <input onChange={handlePhoneNumber} value={phone_number} required placeholder="Phone #" type="number" id="phone_number" name="phone_number" className="form-control" />
                                            <label htmlFor="name">Phone Number</label>
                                        </div>
                                    </div>
                                </div>
                                <button className="btn btn-lg btn-primary">Create Profile</button>
                            </form>
                            <div className="alert alert-success d-none mb-0" id="success-message">
                Congratulations!</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );

}

export default CustomerForm;
