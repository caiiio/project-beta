import React, { useEffect, useState } from 'react';

function SalesList({ sales, fetchSales }) {

    console.log(sales)
    const deleteSale = async (sale) => {
        const saleUrl = `http://localhost:8090/api/sales/${sale.id}/`;
        const fetchConfig = {
            method: "delete",
            headers: {
                'Content-Type': 'application/json',
            }
        };
        const response = await fetch(saleUrl, fetchConfig)
        if (response.ok) {
            fetchSales()
        }
    }


    const availableSale = async (sale) => {
        const saleUrl = `http://localhost:8090/api/sales/${sale.id}/`;
        const fetchConfig = {
            method: "put",
            body: JSON.stringify({ completed: true }),
            headers: {
                'Content-Type': 'application/json',
            }
        };
        const response = await fetch(saleUrl, fetchConfig)
        if (response.ok) {
            fetchSales()
        }
    }


    function filterSales(sales) {
        const results = []
        for (let sale of sales) {
            if (sale["availability"] != true) {
                results.push(sale)
            }
        }
        return results;
    }

    sales = filterSales(sales)

    return (
        <>
            <center>
                <h1>Sales</h1>

            </center>

            <table className="table table-striped table-hover align-middle mt-5">

                <thead>
                    <tr>
                        <th>Sales Person</th>
                        <th>Customer</th>
                        <th>VIN</th>
                        <th>Sale Price</th>
                        <th>Cancel</th>
                        <th>Available</th>
                    </tr>
                </thead>
                <tbody>
                    {sales.map(sale => {
                        return (
                            <tr key={sale.id}>
                                <td>{sale.sales_person.name}</td>
                                <td>{sale.customer.name}</td>
                                <td>{sale.vehicle.vin}</td>
                                <td>{sale.price}</td>
                                <td>
                                    <button className="btn btn-outline-danger" onClick={() => deleteSale(sale)}>Cancel</button>
                                </td>
                                <td>
                                    <button className="btn btn-outline-success" onClick={() => availableSale(sale)}>Sold</button>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </>

    )
}

export default SalesList;
