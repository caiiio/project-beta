import React, { useEffect, useState } from 'react';

function AppointmentList({ appointments, fetchAppointments }) {

    console.log(appointments)
    const deleteAppointment = async (appointment) => {
        const appointmentUrl = `http://localhost:8080/api/appointments/${appointment.id}/`;
        const fetchConfig = {
            method: "delete",
            headers: {
                'Content-Type': 'application/json',
            }
        };
        const response = await fetch(appointmentUrl, fetchConfig)
        if (response.ok) {
            fetchAppointments()
        }
    }


    const finishedAppointment = async (appointment) => {
        const appointmentUrl = `http://localhost:8080/api/appointments/${appointment.id}/`;
        const fetchConfig = {
            method: "put",
            body: JSON.stringify({ completed: true }),
            headers: {
                'Content-Type': 'application/json',
            }
        };
        const response = await fetch(appointmentUrl, fetchConfig)
        if (response.ok) {
            fetchAppointments()
        }
    }


    function filterAppointments(appointments) {
        const results = []
        for (let appointment of appointments) {
            if (appointment["finished"] != true) {
                results.push(appointment)
            }
        }
        return results;
    }

    appointments = filterAppointments(appointments)

    return (
        <>
            <center>
                <h1>Appointments</h1>

            </center>

            <table className="table table-striped table-hover align-middle mt-5">

                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Customer name</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th>Cancel</th>
                        <th>Finished</th>
                    </tr>
                </thead>
                <tbody>
                    {appointments.map(appointment => {
                        return (
                            <tr key={appointment.id}>
                                <td>{appointment.vin}</td>
                                <td>{appointment.customer_name}</td>
                                <td>{new Date(appointment.datetime).toLocaleDateString()}</td>
                                <td>{new Date(appointment.datetime).toLocaleTimeString("en-US", { hour: '2-digit', minute: '2-digit' })}</td>
                                <td>{appointment.technician.name}</td>
                                <td>{appointment.reason}</td>
                                <td>
                                    <button className="btn btn-outline-danger" onClick={() => deleteAppointment(appointment)}>Cancel</button>
                                </td>
                                <td>
                                    <button className="btn btn-outline-success" onClick={() => finishedAppointment(appointment)}>Finished</button>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </>

    )
}

export default AppointmentList;
