import React, {useEffect, useState } from 'react';

function VehicleModelForm() {
    const [manufacturers, setManufacturers] = useState([])

    const [name, setNames] = useState('')
    const Namechange = (event) => {
        const value = event.target.value;
        setNames(value)
    }

    const [picture, setPicture] = useState('')
    const picturechange = (event) => {
        const value = event.target.value;
        setPicture(value)
    }

    const [manufacturer, setManufacturer] = useState('')
    const manufacturerchange = (event) => {
        const value = event.target.value;
        setManufacturer(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {}
        data.name = name
        data.picture_url = picture
        data.manufacturer_id = manufacturer

        const modelURL = 'http://localhost:8100/api/models/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }
        const response = await fetch(modelURL, fetchConfig)
        if (response.ok) {
            const newModel = await response.json();
            console.log(newModel);

            setNames('');
            setPicture('');
            setManufacturer('');
        }
    }

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/manufacturers/';

        const response = await fetch(url);

        if (response.ok) {
          const data = await response.json();
          setManufacturers(data.manufacturers)


        }
      }

      useEffect(() => {
        fetchData();
      }, []);

  return (
    <div className="my-5 container">
            <div className="row">
              <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                  <div className="card-body">
                    <form onSubmit={handleSubmit} id="create-model-form">
                      <h1 className="card-title">Create a Vehicle Model</h1>

                      <div className="row">
                        <div className="col">
                          <div className="form-floating mb-3">
                            <input onChange={Namechange}  required placeholder="Name" type="text" id="name" name="name" className="form-control" value={name}/>
                            <label htmlFor="name">Name</label>
                          </div>
                          <div className="form-floating mb-3">
                            <input onChange={picturechange} placeholder="Picture URL" required type="url" name ="picture_url" id="picture_url" className="form-control" value={picture}/>
                            <label htmlFor="picture">Image URL</label>
                          </div>
                          <div className="mb-3">
                            <select onChange={manufacturerchange} required id="manufacturer" name="manufacturer" className="form-select" value={manufacturer}>
                            <option value="">Manufacturer</option>
                            {manufacturers.map(manufacture => {
                                return (
                            <option key={manufacture.href} value={manufacture.id}>
                            {manufacture.name}
                            </option>
                );
                })}
                </select>
              </div>
                        </div>


                      </div>
                      <button className="btn btn-lg btn-primary">Create Vehicle</button>
                    </form>
                  </div>
                </div>
              </div>
             </div>
          </div>
  )
}

export default VehicleModelForm;
