import React, { useEffect, useState } from 'react';

function TechnicianForm(props) {
    const [name, setName] = useState('');
    const [employeeNumber, setEmployeeNumber] = useState('');

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const handleEmployeeNumberChange = (event) => {
        const value = event.target.value;
        setEmployeeNumber(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};

        data.name = name;
        data.employee_number = employeeNumber;

        const technicianUrl = "http://localhost:8080/api/technicians/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };

        const response = await fetch(technicianUrl, fetchConfig);
        if (response.ok) {
            const newTechnician = await response.json();

            setName("");
            setEmployeeNumber("");
        }
    }

    return (
        <div className="my-5 container">
            <div className="row">
                <div className="col col-sm-auto">
                </div>
                <div className="col">
                    <div className="card shadow">
                        <div className="card-body">
                            <form onSubmit={handleSubmit} id="create-technician-form">
                                <h1 className="card-title">New Technician Form</h1>
                                <div className="row">
                                    <div className="col">

                                        <div className="form-floating mb-3">
                                            <input onChange={handleNameChange} value={name} required placeholder="technician name" type="text" id="name" name="name" className="form-control" />
                                            <label htmlFor="name">Name</label>
                                        </div>

                                        <div className="form-floating mb-3">
                                            <input onChange={handleEmployeeNumberChange} value={employeeNumber} required placeholder="employee number" type="employee_number" id="employee_number" name="employee_number" className="form-control" />
                                            <label htmlFor="employee_number">Employee ID</label>
                                        </div>
                                    </div>
                                </div>
                                <button className="btn btn-lg btn-primary">Create Technician</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );

}

export default TechnicianForm;
