import React from 'react';

function AutomobileList({automobiles, manufacturers, vehiclemodels}) {

    return (
        <table className="table table-striped table-hover align-middle mt-5">
          <thead>
            <tr>
              <th>Year</th>
              <th>Color</th>
              <th>Manufacturer</th>
              <th>Model</th>
              <th>Color</th>
              <th>VIN</th>

            </tr>
          </thead>
          <tbody>
            {automobiles.map(automobile => {
                return (
                    <tr key={automobile.id}>
                        <td>{ automobile.year }</td>
                        <td>{ automobile.color }</td>
                        <td>{ automobile.model.manufacturer.name }</td>
                        <td>{ automobile.vehiclemodels.name }</td>
                        <td>{ automobile.vin }</td>
                    </tr>
                );
            })}
          </tbody>
        </table>

    )
}

export default AutomobileList;
