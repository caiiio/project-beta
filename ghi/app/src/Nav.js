import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">

            <li className="nav-item"><NavLink className="nav-link" to="/manufacturers/">List Manufacturers</NavLink></li>

            <li className="nav-item"><NavLink className="nav-link" to="/manufacturers/new">Add Manufacturer</NavLink></li>

            <li className="nav-item"><NavLink className="nav-link" to="/vehiclemodels/">List Vehicle Models</NavLink></li>

            <li className="nav-item"><NavLink className="nav-link" to="/vehiclemodels/new">Add Vehicle Model</NavLink></li>

            <li className="nav-item"><NavLink className="nav-link" to="/automobiles/">List Automobiles</NavLink></li>

            <li className="nav-item"><NavLink className="nav-link" to="/automobiles/new">Add Automobile</NavLink></li>

            <li className="nav-item"><NavLink className="nav-link" to="/customers/new">Add Customer</NavLink></li>

            <li className="nav-item"><NavLink className="nav-link" to="/employees/new">Add Salesperson</NavLink></li>

            <li className="nav-item"><NavLink className="nav-link" to="/sales/new">Add Sales</NavLink></li>

            <li className="nav-item"><NavLink className="nav-link" to="/sales/">List Sales</NavLink></li>

            <li className="nav-item"><NavLink className="nav-link" to="/technicians/new">Add Technician</NavLink></li>

            <li className="nav-item"><NavLink className="nav-link" to="/appointments/new">Add Service Appointment</NavLink></li>

            <li className="nav-item"><NavLink className="nav-link" to="/appointments/">List Appointments</NavLink></li>

          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
