import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';

import ManufacturerForm from './ManufacturerForm';
import ManufacturerList from './ManufacturerList';

import VehicleModelForm from './VehicleModelForm';
import VehicleModelList from './VehicleModelList';

import AutomobileForm from './AutomobileForm';
import AutomobileList from './AutomobileList';

import TechnicianForm from './TechnicianForm';
import AppointmentForm from './AppointmentForm';
import AppointmentList from './AppointmentList';

import CustomerForm from './CustomerForm';
import SalesPersonForm from './SalesPersonForm';
import SalesForm from './SalesForm';
import SalesList from './SalesList';

import { useState, useEffect } from "react";

function App(props) {
  const [manufacturers, setManufacturers] = useState([])
  const [vehiclemodels, setVehicleModels] = useState([])
  const [automobiles, setAutomobiles] = useState([])
  const [technicians, setTechnicians] = useState([])
  const [appointments, setAppointments] = useState([])
  const [sales, setSales] = useState([])
  const [salesperson, setSalesPerson] = useState([])
  const [customer, setCustomer] = useState([])

  const getManufacturers = async () => {
    const url = 'http://localhost:8100/api/manufacturers/'
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      const manufacturers = data.manufacturers
      console.log(manufacturers, "manufacturers")
      setManufacturers(manufacturers)
    }
  }

  const getVehicleModels = async () => {
    const url = 'http://localhost:8100/api/models/'
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      const vehiclemodels = data.models
      setVehicleModels(vehiclemodels)
    }
  }

  const getAutomobiles = async () => {
    const url = 'http://localhost:8100/api/automobiles/'
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      const automobiles = data.autos
      setAutomobiles(automobiles)
      console.log(automobiles, "automobiles!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
    }
  }

  const getTechnicians = async () => {
    const url = 'http://localhost:8080/api/technicians/'
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      const technicians = data.technicians
      setTechnicians(technicians)
    }
  }

  const getAppointments = async () => {
    const url = 'http://localhost:8080/api/appointments/'
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      const appointments = data.appointments
      setAppointments(appointments)
    }
  }

  const getSales = async () => {
    const url = 'http://localhost:8090/api/sales/'
    const response = await fetch(url);
    console.log("returning for debug: ", response)
    if (response.ok) {
      const data = await response.json();
      const sales = data.sales
      setSales(sales)
    }
  }

  const getSalesPerson = async () => {
    const url = 'http://localhost:8090/api/employees/'
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      const salesperson = data.employees
      setSalesPerson(salesperson)
    }
  }

  const getCustomers = async () => {
    const url = 'http://localhost:8090/api/customers/'
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      const customers = data.customers
      setCustomer(customers)
    }
  }

  useEffect(() => {
    getManufacturers();
    getVehicleModels();
    getAutomobiles();
    getSales();
    getTechnicians();
    getAppointments();
    getSalesPerson();
    getCustomers();


  }, [])

  console.log(appointments)

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="manufacturers/new" element={<ManufacturerForm manufacturers={manufacturers}/>} />
          <Route path="manufacturers/" element={<ManufacturerList manufacturers={manufacturers}/>} />
          <Route path="vehiclemodels/new" element={<VehicleModelForm vehiclemodels={vehiclemodels} manufacturers={manufacturers}/>} />
          <Route path="vehiclemodels/" element={<VehicleModelList vehiclemodels={vehiclemodels} manufacturers={manufacturers}/>} />
          <Route path="automobiles/new" element={<AutomobileForm vehiclemodel={vehiclemodels} manufacturers={manufacturers}/>} />
          <Route path="automobiles/" element={<AutomobileList automobiles={automobiles}vehiclemodels={vehiclemodels} manufacturers={manufacturers}/>} />
          <Route path="technicians/new" element={<TechnicianForm />} />
          <Route path="appointments/new" element={<AppointmentForm technicians={technicians}/>} />
          <Route path="appointments/" element={<AppointmentList appointments={appointments} />} />
          <Route path="employees/new" element={<SalesPersonForm />} />
          <Route path="customers/new" element={<CustomerForm />} />
          <Route path="sales/new" element={<SalesForm customer={customer} salesperson={salesperson}/>} />
          <Route path="sales/" element={<SalesList sales={sales} />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
