import React, { useEffect, useState } from 'react';

function SalesForm(props) {
    const [price, setPrice] = useState('');
    const [sales_person, setEmployeeNumber] = useState('');
    const [customer, setCustomerNumber] = useState('');
    const [vehicle, setVehicleNumber] = useState('');

    const handlePriceChange = (event) => {
        const value = event.target.value;
        setPrice(value);
    }

    const handleEmployeeNumberChange = (event) => {
        const value = event.target.value;
        setEmployeeNumber(value);
    }

    const handleCustomerNumberChange = (event) => {
        const value = event.target.value;
        setCustomerNumber(value);
    }

    const handleVehicleNumberChange = (event) => {
        const value = event.target.value;
        setVehicleNumber(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};

        data.price = price;
        data.sales_person = sales_person;
        data.customer = customer;
        data.vehicle = vehicle;

        const saleUrl = "http://localhost:8090/api/sales/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };

        const response = await fetch(saleUrl, fetchConfig);
        if (response.ok) {
            const newSalesperson = await response.json();

            setPrice("");
            setEmployeeNumber("");
            setCustomerNumber("");
            setVehicleNumber("");
        }
    }

    return (
        <div className="my-5 container">
            <div className="row">
                <div className="col col-sm-auto">
                </div>
                <div className="col">
                    <div className="card shadow">
                        <div className="card-body">
                            <form onSubmit={handleSubmit} id="create-employee-form">
                                <h1 className="card-title">New Transaction</h1>
                                <div className="row">
                                    <div className="col">

                                        <div className="form-floating mb-3">
                                            <input onChange={handlePriceChange} value={price} required placeholder="Enter a price" type="text" id="name" name="name" className="form-control" />
                                            <label htmlFor="name">Price</label>
                                        </div>

                                        <div className="form-floating mb-3">
                                            <input onChange={handleEmployeeNumberChange} value={sales_person} required placeholder="Employee ID" type="employee_id" id="employee_id" name="employee_id" className="form-control" />
                                            <label htmlFor="employee_id">Employee ID</label>
                                        </div>

                                        <div className="form-floating mb-3">
                                            <input onChange={handleCustomerNumberChange} value={customer} required placeholder="Customer ID" type="customer_id" id="customer_id" name="customer_id" className="form-control" />
                                            <label htmlFor="name">Customer ID</label>
                                        </div>

                                        <div className="form-floating mb-3">
                                            <input onChange={handleVehicleNumberChange} value={vehicle} required placeholder="Vehicle ID" type="employee_id" id="employee_id" name="employee_id" className="form-control" />
                                            <label htmlFor="employee_id">Vehicle ID</label>
                                        </div>
                                    </div>
                                </div>
                                <button className="btn btn-lg btn-primary">Create SalesPerson</button>
                            </form>
                            <div className="alert alert-success d-none mb-0" id="success-message">
                Congratulations!</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );

}

export default SalesForm;
