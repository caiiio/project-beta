import React, { useState, useEffect } from 'react'

function AutomobileForm({ vehiclemodel }) {

    const [vehiclemodels, setVehiclesModels] = useState([])

    const [color, setColor] = useState('')
    const colorchange = (event) => {
        const value = event.target.value;
        setColor(value)
    }

    const [year, setYear] = useState('')
    const yearchange = (event) => {
        const value = event.target.value;
        setYear(value)
    }

    const [VIN, setVIN] = useState('')
    const vinchange = (event) => {
        const value = event.target.value;
        setVIN(value)
    }

    const [model, setVehicleModel] = useState('')
    const vehicleModelchange = (event) => {
        const value = event.target.value;
        setVehicleModel(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {}
        data.color = color
        data.year = year
        data.vin = VIN
        data.model_id = model

        const automobileURL = 'http://localhost:8100/api/automobiles/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }
        const response = await fetch(automobileURL, fetchConfig)
        if (response.ok) {
            const newAutomobile = await response.json();
            console.log(newAutomobile);

            setColor('');
            setYear('');
            setVIN('');
            setVehicleModel('')
        }
    }


    const fetchData = async () => {
        const url = 'http://localhost:8100/api/vehiclemodels/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setVehicleModel(data.vehiclemodel)


        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="my-5 container">
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <div className="card-body">
                            <form onSubmit={handleSubmit} id="create-automobile-form">
                                <h1 className="card-title">Add Automobile</h1>

                                <div className="row">
                                    <div className="col">

                                        <div className="form-floating mb-3">
                                            <input onChange={colorchange} value={color} required placeholder="Color" type="text" id="color" name="color" className="form-control" />
                                            <label htmlFor="color">Color</label>
                                        </div>

                                        <div className="form-floating mb-3">
                                            <input onChange={yearchange} placeholder="Year" required type="text" name="year" id="year" className="form-control" value={year} />
                                            <label htmlFor="year">Year</label>
                                        </div>

                                        <div className="form-floating mb-3">
                                            <input onChange={vinchange} placeholder="VIN" required type="text" name="vin" id="vin" className="form-control" value={VIN} />
                                            <label htmlFor="vin">VIN</label>
                                        </div>

                                        <div className="mb-3">
                                            <select onChange={vehicleModelchange} required id="vehiclemodel" name="vehiclemodel" className="form-select" value={vehiclemodels}>
                                                <option value="">Choose Vehicle Model</option>
                                                {vehiclemodel.map(model => {
                                                    return (
                                                        <option key={model.href} value={model.id}>
                                                            {model.name}
                                                        </option>
                                                    );
                                                })}
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <button className="btn btn-lg btn-primary">Create Automobile</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default AutomobileForm;
