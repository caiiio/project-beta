import React, { useEffect, useState } from 'react';

function AppointmentForm({technicians}) {
    const [vin, setVin] = useState('');
    const [customerName, setCustomerName] = useState('');
    const [dateTime, setDateTime] = useState('');
    const [technician, setTechnician] = useState('');
    const [reason, setReason] = useState('');
    const [vip, setVip] = useState('');
    const [finished, setFinished] = useState('');


    const handleVinChange = (event) => {
        const value = event.target.value;
        setVin(value);
    }

    const handleCustomerNameChange = (event) => {
        const value = event.target.value;
        setCustomerName(value);
    }

    const handleDateTimeChange = (event) => {
        const value = event.target.value;
        setDateTime(value);
    }

    const handleTechnicianChange = (event) => {
        const value = event.target.value;
        setTechnician(value);
    }

    const handleReasonChange = (event) => {
        const value = event.target.value;
        setReason(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};

        data.vin = vin;
        data.customer_name = customerName;
        data.date_time = dateTime;
        data.technician = technician;
        data.reason = reason;
        data.finished = finished;


        const appointmentUrl = "http://localhost:8080/api/appointments/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };

        const response = await fetch(appointmentUrl, fetchConfig);
        if (response.ok) {
            const newAppointment = await response.json();

            setVin("");
            setCustomerName("");
            setDateTime("");
            setTechnician("");
            setReason("");
            setFinished("");
        }
    }

    return (
        <div className="my-5 container">
            <div className="row">
                <div className="col col-sm-auto">
                </div>
                <div className="col">
                    <div className="card shadow">
                        <div className="card-body">
                            <form onSubmit={handleSubmit} id="create-appointment-form">
                                <h1 className="card-title">New Appointment Form</h1>
                                <div className="row">
                                    <div className="col">

                                        <div className="form-floating mb-3">
                                            <input onChange={handleVinChange} value={vin} required placeholder="vin" type="text" id="vin" name="vin" className="form-control" />
                                            <label htmlFor="vin">Vin</label>
                                        </div>

                                        <div className="form-floating mb-3">
                                            <input onChange={handleCustomerNameChange} value={customerName} required placeholder="customer name" type="text" id="customer_name" name="customer_name" className="form-control" />
                                            <label htmlFor="customer_name">Customer Name</label>
                                        </div>

                                        <div className="form-floating mb-3">
                                            <input onChange={handleDateTimeChange} value={dateTime} required placeholder="date time" type="datetime-local" id="date_time" name="date_time" className="form-control" />
                                            <label htmlFor="date_time">Date and Time</label>
                                        </div>

                                        <div className="form-floating mb-3">
                                            <input onChange={handleReasonChange} value={reason} required placeholder="reason" type="text" id="reason" name="reason" className="form-control" />
                                            <label htmlFor="reason">Reason</label>
                                        </div>

                                        <div className="mb-3">
                                            <select onChange={handleTechnicianChange} required id="technician" name="technician" className="form-select" >
                                                <option value="">Choose technician</option>
                                                {(technicians).map((technician) => {
                                                    return (
                                                    <option key={technician.id} value={technician.href}>
                                                    {technician.name}
                                                    </option>
                                                    );
                                                })}
                                            </select>
                                        </div>

                                    </div>
                                </div>
                                <button className="btn btn-lg btn-primary">Create Appointment</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );

}

export default AppointmentForm;
