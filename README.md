# CarCar

CarCar is the premiere solution for automobile delaership management. It includes 3 microservices:

-Inventory: includes Manufacturer, Vehicle Model, and Automobile.
-Service: includes Technician and Appointment.
Sales : includes Sale, SalesPerson, and Customer.

Team:

- Caio Lima - Services
- Senna Garfio - Sales

## How to Run this Application

-Fork the repo https://gitlab.com/caiiio/project-beta
-Select clone with HTTPS and copy the link
-Clone the repo in Terminal:
        git clone "your-repo-link"
-Change your directory to the newly cloned repo in Terminal:
        cd "repo-name"
-Open project with VSCode in Terminal:
        code .
-Run in Termnal:
        docker volume create beta-data
        docker-compose build
        docker-compose up
-If you run docker-compose up and if you're on macOS, you'll see
a warning about an environment variable named OS being missing. You can safely ignore this.
-Once docker is up and running, go to http://localhost:3000/ to
see the web app.

## Application Diagram

![Alt text](https://media.discordapp.net/attachments/1042927494816346154/1068700241068560444/project-beta-diagram.png?width%3D763%26height%3D497)

## CRUD Routes

Service:

  HTTP Method         Path           Description
| ----------- | -------------------- |--------------|
| GET         | /api/appointments    | List of appointments
| POST        | /api/appointments    | Create new appointment
| DELETE      | /api/appointments/id | Cancel appointment by ID

| HTTP Method    Path               Description
| -------| ----------------|----------------------- |
| GET    | /api/technicians     | List of technicians
| POST   | /api/technicians     | Create new technician
| DELETE | /api/technicians/id| Delete technician by ID

Sales:

  HTTP Method         Path           Description
| ----------- | -------------------- |--------------|
| GET         | /api/sales           | List of sales
| POST        | /api/sales           | Create new sale
| DELETE      | /api/sales/id        | Delete record by ID

| HTTP Method    Path               Description
| -------| ----------------|----------------------- |
| GET    | /api/employees     | List of salespeople
| POST   | /api/employees     | Create new salesperson
| DELETE | /api/employees/id  | Remove salesperson by ID

| HTTP Method    Path               Description
| -------| ----------------|----------------------- |
| GET    | /api/customers     | List of customers
| POST   | /api/customers     | Create new customer
| DELETE | /api/customers/id  | Remove customers by ID



## API Documentation
Appointment Service:
Localhost, Port 8080


GET request to /api/appointments/

Returns:
{
	"appointments": [
		{
			"href": "/api/appointments/3/",
			"vin": "ABC123ABC123ABC122",
			"customer_name": "Sean",
			"datetime": "2023-12-23T00:00:00+00:00",
			"technician": {
				"name": "Al Capucino",
				"employee_number": 2,
				"id": 4
			},
			"reason": "brakesnotworking",
			"vip": false,
			"finished": false,
			"id": 3
		},

POST request to /api/appointments/

Request body:
{
	"vin": "ABC143ABC164A63C142",
	"customer_name": "mike",
	"datetime": "2023-12-25",
	"technician": 5,
	"reason": "brakes maybe not working"
}

Returns (status code 200):
{
	"href": "/api/appointments/13/",
	"vin": "ABC143ABC164A63C142",
	"customer_name": "mike",
	"datetime": "2023-12-25",
	"technician": {
		"name": "Al Capucino",
		"employee_number": 2,
		"id": 5
	},
	"reason": "brakes maybe not working",
	"vip": false,
	"finished": false,
	"id": 13
}


Sales Service:
Localhost, Port 8090

GET request to /api/sales/

Returns:
{
	"sales": [
		{
			"href": "/api/sales/1",
			"price": 20000,
			"sales_person": {
				"href": "/api/employees/1",
				"name": "Tom Holland",
				"employee_id": 4,
				"id": 1
			},
			"customer": {
				"href": "/api/customers/1",
				"name": "John Smith",
				"address": "1234 E River",
				"phone_number": "1234567890",
				"id": 1
			},
			"vehicle": {
				"color": "red",
				"year": 2012,
				"vin": "1C3CC5FB2AN120174"
			}
		}

POST request to /api/sales/

Request body:
{
	"price": 20000,
	"sales_person": 1,
	"customer": 1,
	"vehicle": 1
}

Returns (status code 200):
{
	"href": "/api/sales/5",
	"price": 20000,
	"sales_person": {
		"href": "/api/employees/1",
		"name": "Tom Holland",
		"employee_id": 4,
		"id": 1
}


GET request to /api/employees/

Returns:

{
	"customers": [
		{
			"href": "/api/customers/1",
			"name": "FixTest1",
			"address": "1234 E River",
			"phone_number": "1234567890",
			"id": 1
		}
	]
}

POST request to /api/employees/

Request body:
{
  "name": "Jeff",
	"employee_id": 2
}

Returns (status code 200):
{
	"href": "/api/employees/2",
	"name": "Jeff",
	"employee_id": 2,
	"id": 2
}

## Design

CarCar is a

## Service microservice

Explain your models and integration with the inventory
microservice, here.

## Sales microservice

The Sales model refers to the Employee and Customer models which are self-contained and polls to the inventory every (20) seconds to find specific vehicles for sale, or sold.

Customers are able to be registered by name, address and phone number.
Employees have a name and employee ID.
the vehicles are mostly identified by their VIN, but also include description by color, year, and availability.

