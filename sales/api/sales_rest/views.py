import json
from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import (
    SalesPerson, # Section 1*
    Customer, # Section 2*
    AutomobileVO,
    Sale # Section 3*
    )
from .encoders import (
    EmployeeDetailEncoder, # 1*
    CustomerDetailEncoder, # 2*
    SaleDetailEncoder, # 3*
    VehicleDetailEncoder,
)

# Create your views here.


#--------------------------------------------------------------
# SECTION 1 (EMPLOYEES)


# Employee List------------------------------------------------

@require_http_methods(["GET","POST"])
def api_employee_list(request):
    if request.method == "GET":
        employees = SalesPerson.objects.all()
        return JsonResponse(
            {"employees": employees},
            encoder=EmployeeDetailEncoder,
            safe=False,
        )
    else:
            content = json.loads(request.body)
            employees = SalesPerson.objects.create(**content)
            return JsonResponse(
                employees,
                encoder=EmployeeDetailEncoder,
                safe=False,
            )

# Employee Detail View-----------------------------------------

@require_http_methods(["DELETE", "GET", "POST"])
def api_employee_detail(request, id):
    if request.method == "GET":
        employee = SalesPerson.objects.get(id=id)
        return JsonResponse(
            employee,
            encoder=EmployeeDetailEncoder,
            safe=False
        )
    elif request.method == "DELETE":
        count, _ = Sale.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})

#--------------------------------------------------------------
# SECTION 2 (CUSTOMERS)


# Customer List------------------------------------------------

@require_http_methods(["GET","POST"])
def api_customer_list(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers":customers},
            encoder=CustomerDetailEncoder,
            safe=False,
        )
    else:
            content = json.loads(request.body)
            customers = Customer.objects.create(**content)
            return JsonResponse(
                customers,
                encoder=CustomerDetailEncoder,
                safe=False,
            )

# Customer Detail View-----------------------------------------

@require_http_methods(["DELETE", "GET", "POST"])
def api_customer_detail(request, id):
    if request.method == "GET":
        customer = Customer.objects.get(id=id)
        return JsonResponse(
            customer,
            encoder=CustomerDetailEncoder,
            safe=False
        )
    elif request.method == "DELETE":
        count, _ = Customer.objects.get(id=id).delete()
        return JsonResponse({"deleted": count > 0})

#--------------------------------------------------------------
# SECTION 3 (SALES)


# Sale List----------------------------------------------------

@require_http_methods(["GET","POST"])
def api_sales_list(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse(
            {"sales":sales},
            encoder=SaleDetailEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        try:
            employee_id = content["sales_person"]
            employee = SalesPerson.objects.get(id=employee_id)
            content["sales_person"] = employee
        except SalesPerson.DoesNotExist:
            return JsonResponse(
                {"message":"Invalid employee id"},
                status=400,
            )
        try:
            customer_id = content["customer"]
            customer = Customer.objects.get(id=customer_id)
            content["customer"] = customer
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message":"Invalid customer id"},
                status=400,
            )
        try:
            vehicle_id = content["vehicle"]
            vehicle = AutomobileVO.objects.get(id=vehicle_id)
            content["vehicle"] = vehicle
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message":"Invalid VIN"},
                status=400,
            )
        sales = Sale.objects.create(**content)
        return JsonResponse(
            sales,
            encoder=SaleDetailEncoder,
            safe=False,
        )




# Sale Detail View----------------------------------------------

@require_http_methods(["DELETE", "GET", "POST"])
def api_sale_detail(request, id):
    if request.method == "GET":
        sale = Sale.objects.get(id=id)
        return JsonResponse(
            sale,
            encoder=SaleDetailEncoder,
            safe=False
        )
    elif request.method == "DELETE":
        count, _ = Sale.objects.get(id=id).delete()
        return JsonResponse({"deleted": count > 0})








@require_http_methods(["GET"])
def api_vehicle_Value_Object_list(request):
    if request.method == "GET":
        vehicles = AutomobileVO.objects.all()
        return JsonResponse(
            vehicles,
            encoder=VehicleDetailEncoder,
            safe=False,
        )