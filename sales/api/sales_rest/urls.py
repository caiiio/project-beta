from django.urls import path

from .views import (
    api_employee_list,
    api_employee_detail,
    api_customer_list,
    api_customer_detail,
    api_sales_list,
    api_sale_detail,
    api_vehicle_Value_Object_list,
)


urlpatterns = [
    path("employees/",
    api_employee_list, name="api_employee_list"

    ),
    path("employees/<int:id>",
    api_employee_detail, name="api_employee_detail"

    ),
    path("customers/",
    api_customer_list, name="api_customer_list"

    ),
    path("customers/<int:id>",
    api_customer_detail, name="api_customer_detail"

    ),
    path("sales/",
    api_sales_list, name="api_sales_list"

    ),
    path("sales/<int:id>",
    api_sale_detail, name="api_sale_detail"

    ),
    path("vehicles/",
    api_vehicle_Value_Object_list, name="api_vehicle_Value_Object_list"

    ),
]