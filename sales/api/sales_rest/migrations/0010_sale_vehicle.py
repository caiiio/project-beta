# Generated by Django 4.0.3 on 2023-01-27 01:18

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('sales_rest', '0009_automobilevo_delete_vinvo'),
    ]

    operations = [
        migrations.AddField(
            model_name='sale',
            name='vehicle',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='vehicle', to='sales_rest.automobilevo'),
        ),
    ]
