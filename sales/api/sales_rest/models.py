from django.db import models
from django.urls import reverse


# Create your models here.



class SalesPerson(models.Model):
    name = models.CharField(max_length=200)
    employee_id = models.PositiveSmallIntegerField()

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse("api_employee_detail", kwargs={"id": self.id})




class Customer(models.Model):
    name = models.CharField(max_length=200)
    address = models.CharField(max_length=200)
    phone_number = models.CharField(max_length=12)
    
    def get_api_url(self):
        return reverse("api_customer_detail", kwargs={"id": self.id})


class AutomobileVO(models.Model):
    color = models.CharField(max_length=50)
    year = models.PositiveSmallIntegerField()
    vin = models.CharField(max_length=17, unique=True)
    availability = models.BooleanField(default=True)
    import_href = models.CharField(max_length=200, unique=True)


class Sale(models.Model):

    price = models.IntegerField(max_length=20, null=True)

    sales_person = models.ForeignKey(
    SalesPerson,
    related_name="sales_person",
    on_delete=models.CASCADE,
    null=True,
)
    customer = models.ForeignKey(
    Customer,
    related_name="customer",
    on_delete=models.CASCADE,
    null=True,
    
)
    vehicle = models.ForeignKey(
    AutomobileVO,
    related_name="vehicle",
    on_delete=models.CASCADE,
    null=True,
    
)

    def get_api_url(self):
        return reverse("api_sale_detail", kwargs={"id": self.id})