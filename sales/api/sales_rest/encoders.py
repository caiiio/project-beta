from common.json import ModelEncoder

from .models import (
    SalesPerson,
    Customer,
    Sale,
    AutomobileVO
    )

class EmployeeDetailEncoder(ModelEncoder):
    model = SalesPerson
    properties = ["name", "employee_id", "id"]

class CustomerDetailEncoder(ModelEncoder):
    model = Customer
    properties = ["name", "address", "phone_number", "id"]

class VehicleDetailEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["color","year","vin"]

class SaleDetailEncoder(ModelEncoder):
    model = Sale
    properties = [
        "price",
        "sales_person",
        "customer",
        "vehicle",
        ]
    encoders={
        "sales_person": EmployeeDetailEncoder(),
        "customer": CustomerDetailEncoder(),
        "vehicle": VehicleDetailEncoder(),
    }
