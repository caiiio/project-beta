import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "service_project.settings")
django.setup()


from service_rest.models import VinVO


def get_vins():
    response = requests.get("http://localhost:8000/api/automobiles/")
    content = json.loads(response.content)
    for vin in content["vins"]:
        VinVO.objects.update_or_create(
            import_href=vin["href"],
            defaults={
                "vin": vin["vin"],
            },
        )

def poll():
    while True:
        print('Service poller polling for data')
        try:
            get_vins()
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
