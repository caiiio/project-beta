from common.json import ModelEncoder
from .models import Technician, Appointment, VinVO


class TechnicianDetailEncoder(ModelEncoder):
    model = Technician
    properties = [
        "name",
        "employee_number",
        "id",
    ]


class AppointmentDetailEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "vin",
        "customer_name",
        "datetime",
        "technician",
        "reason",
        "vip",
        "finished",
        "id",
    ]
    encoders = {
        "technician": TechnicianDetailEncoder(),
    }


class VinVODetailEncoder(ModelEncoder):
    model = VinVO
    properties = [
        "vin",
    ]
