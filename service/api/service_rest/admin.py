from django.contrib import admin
from .models import Technician, Appointment, VinVO


admin.site.register(Technician)
admin.site.register(Appointment)
admin.site.register(VinVO)
