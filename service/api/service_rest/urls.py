from django.urls import path
from .views import api_list_appointments, api_appointment_details, api_list_technicians

urlpatterns = [
    path("technicians/", api_list_technicians, name="api_create_technician"),
    path("appointments/", api_list_appointments, name="api_create_appointment"),
    path("appointments/", api_list_appointments, name="api_list_appointments"),
    path(
        "appointments/<int:pk>/",
        api_appointment_details,
        name="api_appointment_details",
    ),
]
