from django.db import models
from django.urls import reverse


class Technician(models.Model):
    name = models.CharField(max_length=200)
    employee_number = models.PositiveSmallIntegerField()

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse("api_create_technician", kwargs={"id": self.id})


class Appointment(models.Model):
    vin = models.CharField(max_length=20, unique=True)
    customer_name = models.CharField(max_length=200)
    datetime = models.DateTimeField(null=True)
    technician = models.ForeignKey(
        Technician, related_name="appointments", on_delete=models.CASCADE, null=True
    )
    reason = models.CharField(max_length=200)
    vip = models.BooleanField(default=False, null=True)
    finished = models.BooleanField(default=False, null=True)

    def __str__(self):
        return self.vin

    def get_api_url(self):
        return reverse("api_appointment_details", kwargs={"pk": self.pk})


class VinVO(models.Model):
    vin = models.CharField(max_length=20, unique=True)
    import_href = models.CharField(max_length=200, unique=True, null=True)

    def __str__(self):
        return self.vin
